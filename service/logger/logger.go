package logger

import (
	"fmt"
	"net/http"
	"time"
)

type HTTPLogger interface {
	LogRequest(*http.Request)
	LogResponse(*http.Request, *http.Response, error, time.Duration)
}

type Trigger struct {
	trigger http.RoundTripper
	log     HTTPLogger
}

func main() {
	fmt.Println("https://blog.csdn.net/wangshubo1989/article/details/70234319")
}
