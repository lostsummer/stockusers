package handlers

import (
	"TechPlat/stockusers/config"
	"TechPlat/stockusers/util/redis"
	"fmt"
	"log"

	"github.com/pkg/errors"
)

var (
	redisCannotAccess = errors.New("redis can not access")
)

func getRdsCli() (*redis.RedisClient, error) {
	addr := fmt.Sprintf("%s:%d", config.RedisConf.Host, config.RedisConf.Port)
	cli := redis.GetRedisClient(addr, config.RedisConf.DB)
	if cli == nil {
		return nil, redisCannotAccess
	}
	return cli, nil
}

func stocksNum() int {
	cli, err := getRdsCli()
	if err != nil {
		log.Println(err)
		return -1
	}
	keys, err := cli.Keys(config.RedisConf.Prefix + "*")
	if err == nil {
		return len(keys)
	} else {
		return -1
	}

}

func usersNumInStock(stockID string) int64 {
	cli, err := getRdsCli()
	if err != nil {
		log.Println(err)
		return -1
	}
	num, err := cli.ZCard(config.RedisConf.Prefix + stockID)
	if err == nil {
		return num
	} else {
		return -1
	}
}

func uidsInStock(stockID string) []string {
	return []string{}
}

func uidsInStockSlice(stockID string, start int, end int) []string {
	cli, err := getRdsCli()
	if err != nil {
		log.Println(err)
		return nil
	}
	uids, err := cli.ZRange(config.RedisConf.Prefix+stockID, start, end)
	if err == nil {
		return uids
	} else {
		return nil
	}
}
