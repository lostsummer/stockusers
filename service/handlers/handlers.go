package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type respData struct {
	State   int         `json:"state"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Time    int64       `json:"time"`
}

func getRespData() *respData {
	var resp respData
	resp.Time = time.Now().Unix()
	return &resp
}

func getSuccessRespData() *respData {
	resp := getRespData()
	resp.State = 0
	resp.Message = "success"
	return resp
}

func writeJsonResp(w http.ResponseWriter, resp *respData) {
	data, err := json.Marshal(resp)
	if err == nil {
		w.Header().Set("Content-Type", "application/json;charset=UTF-8")
		fmt.Fprint(w, string(data))
	}
}

// handle /stockusers/number
func StockNumber(w http.ResponseWriter, r *http.Request) {

	numberMap := map[string]int{
		"number": stocksNum(),
	}
	resp := getSuccessRespData()
	resp.Data = numberMap
	writeJsonResp(w, resp)
}

// handle /stockusers/{stockid:[0-9]+}/uids
func Uids(w http.ResponseWriter, r *http.Request) {
	stockid := mux.Vars(r)["stockid"]
	start, err := strconv.Atoi(r.URL.Query().Get("start"))
	if err != nil {
		start = 0
	}
	end, err := strconv.Atoi(r.URL.Query().Get("end"))
	if err != nil {
		end = -1
	}
	uids := uidsInStockSlice(stockid, start, end)
	if uids == nil {
		resp := getRespData()
		resp.State = 1
		resp.Message = "no this stock"
		writeJsonResp(w, resp)
		return
	}
	uidsMap := map[string][]string{
		"uids": uids,
	}
	resp := getSuccessRespData()
	resp.Data = uidsMap
	writeJsonResp(w, resp)
}

//handle /stockusers/{stockid:[0-9]+}/number
func UserNumber(w http.ResponseWriter, r *http.Request) {
	stockid := mux.Vars(r)["stockid"]
	userNum := usersNumInStock(stockid)
	if userNum < 0 {
		resp := getRespData()
		resp.State = 1
		resp.Message = "no this stock"
		writeJsonResp(w, resp)
		return
	}
	numberMap := map[string]int64{
		"number": userNum,
	}
	resp := getSuccessRespData()
	resp.Data = numberMap
	writeJsonResp(w, resp)
}
