package service

import (
	"TechPlat/stockusers/config"
	"TechPlat/stockusers/service/handlers"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

var (
	pathHandlerMap = map[string]func(http.ResponseWriter, *http.Request){
		"/stockusers/number":                    handlers.StockNumber,
		"/stockusers/{stockid:[0-9]{7}}/number": handlers.UserNumber,
		"/stockusers/{stockid:[0-9]{7}}/uids":   handlers.Uids,
	}

	redisNotConfig    = errors.New("redis not config")
	redisCannotAccess = errors.New("redis can not access")
)

func Router() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for path, handler := range pathHandlerMap {
		router.HandleFunc(path, handler).Methods("GET")
	}
	return router
}

func ChkRedis() error {
	conf := config.RedisConf
	if conf == nil {
		return redisNotConfig
	}
	if conf.Host == "" {
		return redisNotConfig
	}
	return nil
}
