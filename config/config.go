package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Redis struct {
	Host   string `yaml:"host"`
	Port   int    `yaml:"port"`
	DB     int    `yaml:"db"`
	Prefix string `yaml:"prefix"`
}

type Server struct {
	Enable bool   `yaml:"enable"`
	IP     string `yaml:"ip"`
	Port   int    `yaml:"port"`
}

type Queue struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
	DB   int    `yaml:"db"`
	Key  string `yaml:"key"`
}

type App struct {
	Redis  Redis  `yaml:"redis"`
	Server Server `yaml:"server"`
	Queue  Queue  `yaml:"queue"`
}

var (
	AppConf   *App
	RedisConf *Redis
	SrvConf   *Server
	QueueConf *Queue
)

func init() {
	AppConf, RedisConf, QueueConf = nil, nil, nil
}

func Load(fileName string) (error, *App) {
	var app App
	var content []byte
	var err error
	if content, err = ioutil.ReadFile(fileName); err == nil {
		if err = yaml.Unmarshal(content, &app); err == nil {
			AppConf = &app
			SrvConf = &(app.Server)
			RedisConf = &(app.Redis)
			QueueConf = &(app.Queue)
			return nil, &app
		}
	}
	return err, nil
}
