# 自选股股票--用户数据同步服务



## 整体组成

![](http://ooyi4zkat.bkt.clouddn.com/ssstockusers_README_arch.png)

## 数据上游

### 基准--云自选股全量库

__key__: hset:emoney:user:mystock  __field__: 用户ID

__value:__ json

样例  `hget hset:emoney:user:mystock 2014230623`

```json
[{
	"pid": "710000000",
	"group": "default",
	"stocks": "1000889,1000666,600654,600601,1300004,1300370,600485,600572,600021,1300005,600896,600663,1002753,600301,600993,1000516,1002618",
	"uid": "2015641543",
	"TimeSpan": 1509659567454
}]
```

只取 group 为 "default" 的记录

并过滤掉非10位数字的非法用户ID，以及一年内未登录的用户ID（根据移动部数据组提供的数据库数据得到）

### 增量--用户操作数据队列

`> lpush  EMoney.Mystock:Changes`

```json
{
	"uid": "2015641543",
	"op": "del",
	"stocks": [
		"1000889",
		"1000666"
	],
	"time": 1509659567454
}
```

定义三种操作 ("op"): "set", "add", "del", 对应语义是"重置", "添加", "删除"， 其中 "set" 时， "stocks" 可为空， 例如

```json
{
	"uid": "2015641543",
	"op": "set",
	"stocks": [],
	"time": 1509659567454
}
```



## 数据下游

### 应答数据格式

```json
{
	"state": 0,
	"message": "success",
	"data": {
		"uids": [
			"2006382990",
			"006382990"
		]
	},
	"time": 1509659567454
}
```



### 所有股票数量

http://domain.com/stockusers/number

```json
{
	"state": 0,
	"message": "success",
	"data": {
		"number": 20010
	},
	"time": 1509659567454
}
```



### 某只股票用户数量

http://domain.com/stockusers/0600000/number

```json
{
	"state": 0,
	"message": "success",
	"data": {
		"number": 8002
	},
	"time": 1509659567454
}
```





### 某只股票用户数据

http://domain.com/stockusers/0600000/uids

```json
{
	"state": 0,
	"message": "success",
	"data": {
		"uids": [
			"2006382990",
			"006382990",
             ...
		]
	},
	"time": 1509659567454
}
```



### 某只股票部分用户数据

http://domain.com/stockusers/0600000/uids?start=0&end=1000

```json
{
	"state": 0,
	"message": "success",
	"data": {
		"uids": [
			"2006382990",
			"006382990",
             ...
		]
	},
	"time": 1509659567454
}
```



