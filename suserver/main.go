package main

import (
	"TechPlat/stockusers/config"
	"TechPlat/stockusers/service"
	"TechPlat/stockusers/util/common"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

const (
	defaultConfigFileName = "stockusers.yaml"
)

var (
	baseDir        string
	configFilePath string
)

func init() {
	baseDir = common.GetCurrentDirectory()
}

func parseFlag() {
	flag.StringVar(&configFilePath, "config", "", "config file path")
	flag.Parse()
	if configFilePath == "" {
		configFilePath = baseDir + "/" + defaultConfigFileName
	}
}

func listenSignal() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT)
	for {
		s := <-c
		switch s {
		case syscall.SIGQUIT, syscall.SIGTERM, syscall.SIGINT:
			return
		case syscall.SIGHUP:
			return
		default:
			return
		}
	}
}

func loadConfig() {
	err, _ := config.Load(configFilePath)
	if err != nil {
		log.Fatalln(err)
	}

}

func serve() {
	srvConf := config.SrvConf
	if srvConf.Enable {
		addr := fmt.Sprintf("%s:%d", srvConf.IP, srvConf.Port)
		if err := service.ChkRedis(); err == nil {
			log.Fatalln(http.ListenAndServe(addr, service.Router()))
		} else {
			log.Fatalln(err)
		}
	}
}

func main() {
	parseFlag()
	loadConfig()
	serve()
}
